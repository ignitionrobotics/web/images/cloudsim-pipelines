FROM golang:1.15 AS builder

RUN apt update \
    && apt install -y git unzip

# Install protobuf
RUN curl -OL https://github.com/google/protobuf/releases/download/v3.12.3/protoc-3.12.3-linux-x86_64.zip \
    && unzip protoc-3.12.3-linux-x86_64.zip -d /protoc3

# Download and install Go packages
RUN go get -u golang.org/x/lint/golint google.golang.org/protobuf/cmd/protoc-gen-go \
    && go install google.golang.org/protobuf/cmd/protoc-gen-go

FROM ubuntu:bionic AS pipeline

# Copy protoc-gen-go
COPY --from=builder --chown=0:0 /protoc3/bin /usr/local/bin
COPY --from=builder --chown=0:0 /protoc3/include /usr/local/include

# Copy and prepare Go
ENV GOPATH /go
ENV PATH ${GOPATH}/bin:/usr/local/go/bin:${PATH}
COPY --from=builder --chown=0:0 ${GOPATH}/ ${GOPATH}/
COPY --from=builder --chown=0:0 /usr/local/go /usr/local/go

# Copy Git
COPY --from=builder --chown=0:0 /usr/bin/git* /usr/bin/
COPY --from=builder --chown=0:0 /usr/lib/git-core /usr/lib/git-core